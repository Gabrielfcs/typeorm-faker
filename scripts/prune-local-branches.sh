#!/bin/bash

# Remove local branches where is not to be on remote

for target in $(git branch | tee | grep -Ev "\\*|main|develop")
do
    [[ $(git branch -r | grep $target | wc -l) -gt 0 ]] \
    && echo "Remote branch detected :"$target \
    || (git branch -D $target && echo "["$target"] branch is deleted")
done;
